/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform baibai. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of baibai.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with baibai.
 * 
 * Modified history:
 *   baibai  2020年5月3日 下午10:12:50  created
 */
package com.desktop.web.service.tunnel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desktop.web.core.comenum.FRPProxyType;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.service.remotecpe.NatInfo;
import com.desktop.web.service.remotecpe.NatService;
import com.desktop.web.service.session.Client;
import com.desktop.web.service.session.DeviceSessionService;
import com.desktop.web.uda.entity.Tunnel;

/**
 * 
 *
 * @author baibai
 */
@Service
public class TunnelPrivateService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DeviceSessionService sessionService;

    @Autowired
    private NatService natService;

    @Autowired
    private TunnelService tunnelService;

    /**
     * 判断代理信息是否合法
     * 
     * @param token
     * @param id
     */
    private void checkProxyRPPortHandler(String token, String id) {
        NatInfo natInfo = natService.getPortInfoByUUID(id);
        if (natInfo == null) {
            throw new BusinessException("checkProxyVNCRDPPortHandler error" + token + "," + id);
        }

        Client client = sessionService.getClientByToken(token);
        if (client == null) {
            throw new BusinessException("checkProxyVNCRDPPortHandler error" + token + "," + id);
        }

        if (!client.getDevice().getId().equals(natInfo.getDeviceId())) {
            throw new BusinessException("checkProxyVNCRDPPortHandler error" + token + "," + id);
        }
    }

    /**
     * 判断tunnel是否合法
     * 
     * @param token
     * @param id
     */
    private void checkProxyTunnelPortHandler(String token, String id) {
        Tunnel tunnel = tunnelService.getTunnelById(Long.valueOf(id));
        if (tunnel == null) {
            throw new BusinessException("checkProxyTunnelPortHandler error" + token + "," + id);
        }

        Client client = sessionService.getClientByToken(token);
        if (client == null) {
            throw new BusinessException("checkProxyTunnelPortHandler error" + token + "," + id);
        }

        if (!client.getDevice().getId().equals(tunnel.getDeviceId())) {
            throw new BusinessException("checkProxyTunnelPortHandler error" + token + "," + id);
        }
    }

    /**
     * 验证端口映射是否合法
     * 
     * @param proxyInfo
     * @return
     */
    public Object checkProxyPort(String proxyInfo, int port) {

        String[] proxyname = proxyInfo.split("_");
        FRPProxyType type = FRPProxyType.valueOf(proxyname[0]);
        String token = proxyname[1];
        String id = proxyname[2];
        if (type == FRPProxyType.RP) {
            this.checkProxyRPPortHandler(token, id);
        } else {
            this.checkProxyTunnelPortHandler(token, id);
        }

        logger.info("client proxy check success,proxyInfo:{}", proxyInfo);
        return "ok";
    }
}
